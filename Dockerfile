FROM traefik:v1.7.6-alpine

MAINTAINER EOLE <eole@ac-dijon.fr>

COPY /traefik /etc/traefik
COPY /config /config
